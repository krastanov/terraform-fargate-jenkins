### Cluster
resource "aws_ecs_cluster" "main" {
  name = "example-cluster"
}

### IAM roles
resource "aws_iam_role" "ecs_task_execution_role" {
  name = "jenkins_ecs_task_execution_role"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role       = aws_iam_role.ecs_task_execution_role.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


### Service
resource "aws_ecs_service" "main" {
  name            = "example-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = "1"
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = ["${aws_security_group.jenkins.id}"]
    subnets          = ["${aws_subnet.subnet_a.id}"]
    assign_public_ip = "true"
  }

#  load_balancer {
#    target_group_arn = aws_alb_target_group.app.id
#    container_name   = "app"
#    container_port   = 8080
#  }
#
#  depends_on = [
#    aws_alb_listener.front_end,
#  ]
}


### Task definition
resource "aws_ecs_task_definition" "app" {
  family                   = "app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
 #execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  container_definitions    = <<DEFINITION
[
  {
    "image": "jenkins/jenkins:lts",
    "cpu": 256,
    "memory": 512,
    "name": "jenkins",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080
      }
    ],
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "jenkins",
            "awslogs-region": "eu-central-1",
            "awslogs-stream-prefix": "master"
        }
    }
  }
]
DEFINITION
}

### Logs
resource "aws_cloudwatch_log_group" "jenkins" {
  name              = "jenkins"
  retention_in_days = 7
}

data "aws_iam_role" "ecs_task_execution_role" {
  name = "ecsTaskExecutionRole"
}


### Security groups
resource "aws_security_group" "jenkins" {
  name          = "jenkins_web"
  vpc_id        = aws_vpc.vpc.id
  tags = {
    Name        = "jenkins_web"
  }
  ingress {
    from_port   = 80
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

