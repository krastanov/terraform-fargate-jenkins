# AWS provider		https://www.terraform.io/docs/providers/aws/index.html

provider "aws" {
  region		= var.main["region"]
  shared_credentials_file = var.main["credentials_file"]
  profile		= var.main["profile"]
  version		= "~> 2.0"
}


# VPC			https://www.terraform.io/docs/providers/aws/r/vpc.html
resource "aws_vpc" "vpc" {
  cidr_block		= var.cidr["vpc"]	# cidr_block from variables.tf
  enable_dns_hostnames	= true			# default is false
  enable_dns_support	= true			# default is true

  tags = {
    Name = "vpc"
  }
}

# Internet gataway	https://www.terraform.io/docs/providers/aws/r/internet_gateway.html
resource "aws_internet_gateway" "gw" {
  vpc_id		= aws_vpc.vpc.id
}


# Routing tables	https://www.terraform.io/docs/providers/aws/r/route_table.html
resource "aws_route_table" "rt" {
  vpc_id		= aws_vpc.vpc.id
  route {
    cidr_block		= "0.0.0.0/0"
    gateway_id		= aws_internet_gateway.gw.id
  }
  tags = {
    Name = "rt"
  }
}


# Main route		https://www.terraform.io/docs/providers/aws/r/main_route_table_assoc.html
resource "aws_main_route_table_association" "main_rt" {
  vpc_id		= aws_vpc.vpc.id
  route_table_id	= aws_route_table.rt.id
}


# DHCP Configuration	https://www.terraform.io/docs/providers/aws/r/vpc_dhcp_options.html
resource "aws_vpc_dhcp_options" "dns_resolver" {
  domain_name_servers	= ["AmazonProvidedDNS"]		# add AWS nameservers to /etc/resolve.conf
  domain_name		= "eu-central-1.test.internal"
}


# DHCP Options		https://www.terraform.io/docs/providers/aws/r/vpc_dhcp_options_association.html
resource "aws_vpc_dhcp_options_association" "dns_options_assoc" {
  vpc_id		= aws_vpc.vpc.id
  dhcp_options_id	= aws_vpc_dhcp_options.dns_resolver.id
}

