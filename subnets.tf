# AWS Subnets			https://www.terraform.io/docs/providers/aws/r/subnet.html
resource "aws_subnet" "subnet_a" {
  vpc_id			= aws_vpc.vpc.id
  cidr_block			= var.cidr["subnet_a"]
  availability_zone		= var.az["subnet_a"]
  map_public_ip_on_launch	= true
  tags = {
    Name = "subnet_a"
  }
}

resource "aws_subnet" "subnet_b" {
  vpc_id			= aws_vpc.vpc.id
  cidr_block			= var.cidr["subnet_b"]
  availability_zone		= var.az["subnet_b"]
  map_public_ip_on_launch	= true
  tags = {
    Name = "subnet_b"
  }
}

resource "aws_subnet" "subnet_c" {
  vpc_id			= aws_vpc.vpc.id
  cidr_block			= var.cidr["subnet_c"]
  availability_zone		= var.az["subnet_c"]
  map_public_ip_on_launch	= true
  tags = {
    Name = "subnet_c"
  }
}
