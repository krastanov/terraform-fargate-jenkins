variable "main" {
  type = map
  description = "AWS credentials file location, profile name"
  default = {
    "credentials_file"	= "~/.aws/credentials"
    "profile"		= "terraform"
    "region"		= "eu-central-1"	# Frankfurt
  }
}

variable "az" {
    type = map
    description = "Availability zones in our region"
    default = {
      "subnet_a"        = "eu-central-1a"
      "subnet_b"        = "eu-central-1b"
      "subnet_c"        = "eu-central-1c"
    }
}

variable "cidr" {
    type = map
    description = "IP range for public subnets"
    default = {
      "vpc"		= "10.0.0.0/16"
      "subnet_a"	= "10.0.1.0/24"
      "subnet_b"	= "10.0.2.0/24"
      "subnet_c"	= "10.0.3.0/24"
    }
}
